//Function to verify conditions check box is ticked.
function ischecked() {
  const checked = document.getElementById('conditions').checked;
  if (checked === false) {
    alert('You must agree to terms and conditions to register!');
    return false;
  }
};

//Form validation for sign ups, logins and contact.
$('.ui.form')
    .form({
      firstName: {
        identifier: 'firstName',
        rules: [
          {
            type: 'empty',
            prompt: 'Please enter your first name.',
          },
        ],
      },

      lastName: {
        identifier: 'lastName',
        rules: [
          {
            type: 'empty',
            prompt: 'Please enter your last name.',
          },
        ],
      },

      address: {
        identifier: 'address',
        rules: [
          {
            type: 'empty',
            prompt: 'Please enter your address.',
          },
        ],
      },

      email: {
        identifier: 'email',
        rules: [
          {
            type: 'email',
            prompt: 'Please enter a valid email address.',
            	
          },
        ],
      },

      password: {
        identifier: 'password',
        rules: [
          {
            type: 'minLength[6]',
            prompt: 'Please enter password(6 character minimum).',
          },
        ],
      },

      userMessage: {
        identifier: 'userMessage',
        rules: [
          {
            type: 'empty',
            prompt: 'You must enter a message to contact us.',
          },
        ],
      },
    });

