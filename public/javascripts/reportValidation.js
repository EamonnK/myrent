//initializing controls for page and validating filter buttons.
$('.ui.dropdown').dropdown();

$('.ui.form')
    .form({
      rentStatus: {
        identifier: 'rentStatus',
        rules: [
          {
            type: 'empty',
            prompt: 'Please enter occupancy filter.',
          },
        ],
      },
      resType: {
        identifier: 'resType',
        rules: [
          {
            type: 'empty',
            prompt: 'Please enter type filter.',
          },
        ],
      },
      rentSort: {
        identifier: 'rentSort',
        rules: [
          {
            type: 'empty',
            prompt: 'Please enter rent filter.',
          },
        ],
      },
    });

