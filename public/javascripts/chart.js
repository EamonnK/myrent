var landNames  = new Array();
var rentTotals = new Array();

$(document).ready(function () {
	  getArrays();
	  render(landNames, rentTotals);
});

/**
 *Function  to get arrays of landlords and their rent totals. 
 */
function getArrays() {
  var i = 0;

  $('.landLordName').each(function () {
    landNames[i] = $(this).attr('value');
    i++;
  });

  i = 0;
  $('.rentTotal').each(function () {
    rentTotals[i] = $(this).attr('value');
    i++;
  });
}


/**
 * Function to send data from arrays to chart.
 * 
 * @param landNames
 * @param rentTotals
 */
function render(landNames, rentTotals) {
  let dps = dataArray(landNames, rentTotals);

  $('#chartContainer').CanvasJSChart({

      title: {
        text: 'Landlord rent rolls as % of all rent.',
        fontSize: 24,
      },
      axisY: {
        title: 'Rental income %',
      },
      legend: {
        verticalAlign: 'center',
        horizontalAlign: 'right',
      },
      data: [{
        type: 'pie',
        showInLegend: true,
        toolTipContent: '{label} <br/> {y} %',
        indexLabel: '{y} %',
        dataPoints: dps,
      }],
  });
}

/**
 * Function to format data from rent array to number, 
 * filling required fields for chart. called above.
 * 
 * @param landNames
 * @param rentTotals
 * @returns
 */
function dataArray(landNames, rentTotals) {
  let data = [];
  for (let i = 0; i < landNames.length; i += 1) {
    data.push({ label: landNames[i],
    y: Number(Math.round((rentTotals[i]) * 10) / 10),
    legendText: landNames[i], });
  }
  return data;
}
  

