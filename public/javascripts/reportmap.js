/**
 * Tenant vacant residences map with polygon filter.
 */

let marker;
latlngStr = [];
const pos = [];
let posIndex = 0;
let startAllowed= true;
var map;
let markers = [];

/**
 * Function to initialize (basic)map with markers, populated with data
 * called by ajax from server.
*/
const reportmap = (function () {

  /**
   * Ajax call to server for array of array of markers 
   */
  function initialize() {
      $.get('/Tenants/geolocations', function (data) {
      }).done(function (data) {
        $.each(data, function (index, geoObj)
        {
          console.log(geoObj[0] + ' ' + geoObj[1] + ' ' + geoObj[2] + ' ' + geoObj[3]);
          
        });
        populateTable(data);
        positionMarkers(data);
      });
    };

  /**
   * For each looping through array adding each element
   * to an array passed to basic map function.
   * 
   * @param data
   */
  function positionMarkers(data) {
    $.each(data, function (index, geoObj) {
      latlngStr.push(geoObj);
    });

    basicMap(latlngStr);
  }

  /**
   * Funtcion to split latlng string and format to number,
   * passing values to new google map LatLng
   * 
   * @param str
   * @returns {google.maps.LatLng}
   */
  function getLatLng(str) {
    const lat = Number(str[1]);
    const lon = Number(str[2]);
    return new google.maps.LatLng(lat, lon);
  }

  /**
   * Initializes map. Takes in latlngStr, loops through each index
   * and sets each marker to map, adding each to markers [].
   * 
   * @param latlngStr
   */
  function basicMap(latlngStr) {
    var center = new google.maps.LatLng(53.3702463, -7.4164152);

    const mapProp = {
      center: center,
      zoom: 7,
      mapTypeId: google.maps.MapTypeId.ROADMAP,
    };
    mapDiv = document.getElementById('map_canvas');
    map = new google.maps.Map(mapDiv, mapProp);
    mapDiv.style.width = '100%';
    mapDiv.style.height = '368px';

    
    for (i = 0; i < latlngStr.length; i++){
    	const infowindow = new google.maps.InfoWindow();
      title = 'Eircode ' + latlngStr[i][0] + ' : ' + latlngStr[i][3];
      marker = new google.maps.Marker({
        position: getLatLng(latlngStr[i]),
        map: map,
        title: title,
      });
      google.maps.event.addListener(marker, 'click', (function (marker, i) {
          return function () {
            infowindow.setContent('Eircode ' + latlngStr[i][0] + " : " + latlngStr[i][3]);
              infowindow.open(map, marker);
          }
      })(marker, i));
      markers.push(marker);
    }
  }

  /**
   * Refresh method for updating markers upon change or termination
   * of tenancy.
   * 
   * @param data
   */
  function updateMarkers(data) {
	
    removeMarkers();
    markers = [];
    latlngStr = [];
    $.each(data, function (index, geoObj)
    {
      latlngStr.push(geoObj);
    });

    for (i = 0; i < latlngStr.length; i++)
    {
      title = 'Eircode ' + latlngStr[i][0];
      marker = new google.maps.Marker({
        position: getLatLng(latlngStr[i]),
        map: map,
        title: title,
      });
      markers.push(marker);
    }
  }
  
  /**
   * Function to remove all markers from map on refresh,
   * preparing for refreshed markers.
   */
  function removeMarkers() {
    for (i = 0; i < markers.length; i += 1)  {
      markers[i].setMap(undefined);
    }
  }

  google.maps.event.addDomListener(window, 'load', initialize);

  return {
    updateMarkers: updateMarkers,
  };
}());

/**
 * Function(onclick) to empty filter table and allow user to draw polygon
 * search.
 */
function start() {
  if (startAllowed == false) {
	alert("Reset to Start");
	return;
  }
  $('#filterTable').empty();
  listenerHandler = google.maps.event.addListener(map, 'click', function (e) {
    pos[posIndex] = e.latLng;
    if (posIndex > 0) {
      polyline(posIndex - 1, posIndex);
    }

    posIndex += 1;
  });

}

/**
 * Function(onclick) to stop polygon drawing and close polygon,
 * using draw polygon function to close shape with a polyline
 * back to position of starting location.
 */
function stop() {
  polyline(pos.length - 1, 0);
  drawPolygon();
  google.maps.event.removeListener(listenerHandler);
  listenerHandler = null;
};


/**
 * Function(onclick) to check if marker location lives within polygon 
 * and if not set 
 */
function filter() {
  latlng = [];
  for (let i = 0; i < latlngStr.length; i += 1) {
    const point = new google.maps.LatLng(latlngStr[i][1], latlngStr[i][2]);
    if (google.maps.geometry.poly.containsLocation(point, polygon)) {
      markers[i].setVisible(true);
      latlng.push(latlngStr[i]);
    } else {
      markers[i].setVisible(false);
    }
  }

  populateTable(latlng);
}

/**
 * Function(onclick) to reload page and allow new filter search
 */
function reset() {
  location.reload();
}

/**
 * Function to populate table with filtered data,
 * using method below to populate rows.
 */
function populateTable(latlng)
{
  $.each(latlng, function (i, val) {
    populateTableRow(val);
  });
}

/**
 * Function to create table row populated with residence details.
 * 
 * @param data array comprising eircode, date, type, no. beds/baths, rent, area)
 */
function populateTableRow(data) {
  const eircode = "<td>" + data[0] + "</td>";
  const date    = "<td>" + data[4] + "</td>";
  const type    = "<td>" + data[5] + "</td>";
  const beds    = "<td>" + data[6] + "</td>";
  const baths   = "<td>" + data[7] + "</td>";
  const rent    = "<td>" + data[8] + "</td>";
  const area    = "<td>" + data[9] + "</td>";
  const land    = "<td>" + data[10] + "</td>";
  const ten     = "<td>" + data[3] + "</td>";
  $('#filterTable').append("<tr>" + eircode + date + type + beds + baths + rent + area + land + ten +"</tr>");
}



/**
 * Function to draw a line of polygon using array with
 *  two latlng positions, previous and current and setting it to map.
 *  
 * @param prevIndex
 * @param index
 */
function polyline(prevIndex, index) {
  const coords = [
    new google.maps.LatLng(pos[prevIndex].lat(), pos[prevIndex].lng()),
    new google.maps.LatLng(pos[index].lat(), pos[index].lng()),];

  const line = new google.maps.Polyline({
    path: coords,
    geodesic: true,
    strokeColor: '#2447FF',
    strokeOpacity: 1.0,
    strokeWeight: 2,
  });
  line.setMap(map);
}

/**
 * Use data (pos[]) to draw polygon, closing it to the
 * point of origin.
 */
function drawPolygon() {
  const lineCoords = [];
  for (let j = 0; j < pos.length; j += 1) {
    console.log(pos[j].lat + ' ' + pos[j].lng);
    lineCoords[j] = new google.maps.LatLng(pos[j].lat(), pos[j].lng());
  }

  lineCoords[pos.length] = new google.maps.LatLng(pos[0].lat(), pos[0].lng());

  polygon = new google.maps.Polyline({
    path: lineCoords,
    geodesic: true,
    strokeColor: '#2447FF',
    strokeOpacity: 1.0,
    strokeWeight: 2,
  });

  polygon.setMap(map);
  google.maps.event.clearListeners(map, 'click');
}
