// initialize the controls in the input data template and validate residence type
$('.ui.dropdown').dropdown();
$('.ui.checkbox').checkbox();
$('.ui.form')
    .form({

      rent: {
        identifier: 'rent',
        rules: [
          {
            type: 'integer[1..10000000]',
            prompt: 'Please enter rent amount(Numbers only).',
          },
        ],
      },

      residenceType: {
        identifier: 'residenceType',
        rules: [
          {
            type: 'empty',
            prompt: 'Please select a residence type',
          },
        ],
      },

      numberBedrooms: {
        identifier: 'numberBedrooms',
        rules: [
          {
            type: 'integer[1..100]',
            prompt: 'Please enter number of bedrooms(Numbers only).',
          },
        ],
      },

      numberBathrooms: {
        identifier: 'numberBathrooms',
        rules: [
          {
            type: 'integer[1..100]',
            prompt: 'Please enter number of bathrooms(Numbers only).',
          },
        ],
      },

      area: {
        identifier: 'area',
        rules: [
          {
            type: 'integer[1..10000]',
            prompt: 'Please enter area(Numbers only).',
          },
        ],
      },

      eircode: {
        identifier: 'eircode',
        rules: [
          {
            type: 'empty',
            prompt: 'Please enter your properties eircode.',
          },
        ],
      },
    });
