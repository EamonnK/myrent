//$(document).ready(function () {
const ADMIN_MAP = (function() { 
  let map;
  const markerLatLng = [];
  let marker;
  const markers = [];
  function initialize() {
    // create basic map without markers
    basicMap();
    // get marker locations and render on map
      retrieveMarkerLocations();
  }

  //retrieves residence info from server
  function retrieveMarkerLocations()
  {
    const latlng = [];
      $(function() {
          $.get("/Administrators/geolocations", function(data) {
          }).done(function(data) {
               $.each(data, function(index, geoObj) 
               {
                     console.log(geoObj[0] + " " + geoObj[1] + " " + geoObj[2] + " " + geoObj[3]);
               });
               
               positionMarkers(data);
               
          });
      });
   }
      
  /**
   * we've got the marker location from data in ajax call
   * we now put data into an array
   * the format is 'zzz zzz, xx.xxxx, yy.yyyyy, sssssss ' -> (eircode, lat, lng, tenant)
   * then invoke 'fitBounds' to render the markers, centre map and create infoWindow to display firstName
   */
  function positionMarkers(data)
  {
    //removeMarkers();
    latlngStr = [];
    $.each(data, function(index, geoObj) 
    {
        latlngStr.push(geoObj);
        });
        fitBounds(latlngStr);
  }
  
/**
 * Initializing basic map without markers
 */
  function basicMap() {
	  const mapProp = {
	    center: new google.maps.LatLng(53.347298,-6.268344),
	    zoom: 7,
	    mapTypeId: google.maps.MapTypeId.ROADMAP,
	  };
	  var mapDiv = document.getElementById('map');
	  mapDiv.style.height = '550px';
	  map = new google.maps.Map(mapDiv, mapProp);
  }
  
  /**
   * formating string into latitude and longitude.
   * 
   * @param str
   * @returns {google.maps.LatLng}
  */
  function getLatLng(str)
  { 
  
    const lat = Number(str[1]);
    const lon = Number(str[2]);
    return new google.maps.LatLng(lat, lon);
  }
   
  /**
   * Function to iterate through markers and add 
   * them to map with populated info windows
   * 
   * @param latlngStr
   */
  function fitBounds(latlngStr)
  {
      const infowindow = new google.maps.InfoWindow();
      
      for (i = 0; i < latlngStr.length; i++) 
      {
        marker = new google.maps.Marker({
            position: getLatLng(latlngStr[i]),
            map: map
        });
          /*respond to click on marker by displaying user (donor) name */
        google.maps.event.addListener(marker, 'click', (function (marker, i) {
            return function () {
              infowindow.setContent('Eircode ' + latlngStr[i][0] + " : " + latlngStr[i][3]);
                infowindow.open(map, marker);
            }
        })(marker, i));
        
        
        
        markers.push(marker); // to facilitate removel of markers
      }
  }


  //update markers on deletion using response from ajax.
  function updateMarkers(data)
  {
    removeMarkers();
    latlngStr = [];
    $.each(data, function(index, geoObj) 
    {
        latlngStr.push(geoObj);
     });
  
    const infowindow = new google.maps.InfoWindow();
    
    for (i = 0; i < latlngStr.length; i++) 
    {
        marker = new google.maps.Marker({
            position: getLatLng(latlngStr[i]),
            map: map
        });
        
        
        google.maps.event.addListener(marker, 'click', (function (marker, i) {
            return function () {
              infowindow.setContent('Eircode ' + latlngStr[i][0] + " : " + latlngStr[i][3]);
                infowindow.open(map, marker);
            }
        })(marker, i));
                  
        markers.push(marker); 
    }
  }
  
  function removeMarkers()  
  {
    for(i = 0; i < markers.length; i += 1)  {
      markers[i].setMap(undefined);
    }
  }
  
  google.maps.event.addDomListener(window, 'load', initialize);

  return {
	    updateMarkers : updateMarkers
	  } 
}());
 


