$('.ui.dropdown').dropdown();
//Form validation for landlord config page, edit and delete res drop downs.
$('.ui.form.resIdDeleteEdit')
    .form({
      fields:{
      resId: {
        identifier: 'resId',
        rules: [
          {
            type: 'empty',
            prompt: 'Please select a residence.',
          },
        ],
      },
      },
     });

//Form validation for Administrator delete tenant dropdown, returning function(ajax)on success and preventing reload.
$('.ui.form.segment.ten')
   .form({  
	  fields:{
      tenId: {
        identifier: 'tenId',
        rules: [
          {
            type: 'empty',
            prompt: 'Please select a tenant.',
          },
        ],
      },
     },
      onSuccess: function(event, fields){
    	  deleteTen();
    	  event.preventDefault();
      }
    });
    
//Form validation for Administrator delete landlord dropdown, returning function(ajax) on success and preventing reload.
$('.ui.form.segment.land')
  .form({
		fields:{
			landId: {
		        identifier: 'landId',
		        rules: [
		          {
		            type: 'empty',
		            prompt: 'Please select a landlord.',
		          },
		        ],
		      },
		},
		onSuccess: function(event, fields){
			  deleteLand();
	    	  event.preventDefault();
	      }
		
		});

//Form validation for Tenant vacantres dropdown, returning function(ajax) on success and preventing reload.
$('.ui.form.segment.tenantChange')
.form({  
	  fields:{
   resId: {
     identifier: 'resId',
     rules: [
       {
         type: 'empty',
         prompt: 'Please select a residence.',
       },
     ],
   },
  },
   onSuccess: function(event, fields){
 	  changeTenancy();
 	  event.preventDefault();
   }
 });

//Form validation for Tenant terminate residence returning function(ajax) and preventing reload.
$('.ui.form.segment.terminate')
.form({  
	  
     onSuccess: function(event){
 	  deleteTenancy();
 	  event.preventDefault();
   }
 });

//Validation for editresidence page, ensuring rent value is number.
function check() {
  const checkNumber = document.getElementById('numberCheck').value;
  if (isNaN(checkNumber)) {
    alert('Rent can only be numeric value. No other characters.');
    return false;
  }
}

//Validation for Landlord edit profile page, password entered must match existing password to update profile.
function passVal() {
  const landPass = document.getElementById('landLord').value;
  const editPass = document.getElementById('password').value;
  if (landPass !== editPass) {
    alert('You must enter your password to apply your updates!');
    return false;
  }
}

//Function for Administrator delete landlord, using ajax call.
function deleteLand() {
    var formData = $('.ui.form.segment.land input').serialize(); 
    $.ajax({
      type : 'POST',
      url : '/Administrators/deleteLandlord',
      data : formData,
      success: 
    	  function(response){
    	  let id = $('#LandLord').dropdown('get value');
	      removeLandlord(id);      
		  ADMIN_MAP.updateMarkers(response);
      }
    });
  }

//Function for Administrator removing deleted landlord from dropdown list.
function removeLandlord(id) {
    let $obj = $('.item.land');
    for (let i = 0; i < $obj.length; i += 1) {
      if($obj[i].getAttribute('data-value').localeCompare(id) == 0) {
        $obj[i].remove();
        $('#LandLord').dropdown('clear');
        break;
      }
    }
  }

//Function for Administrator delete tenant using ajax call.
function deleteTen() {
    var formData = $('.ui.form.segment.ten input').serialize(); 
    $.ajax({
      type : 'POST',
      url : '/Administrators/deleteTenant',
      data : formData,
      success: 
    	  function(response){
    	  let id = $('#Tenant').dropdown('get value');
	      removeTenant(id);      
		  ADMIN_MAP.updateMarkers(response);
      }
    });
  }

//Function for Administrator removing tenant from dropdown list.
function removeTenant(id) {
    let $obj = $('.item.ten');
    for (let i = 0; i < $obj.length; i += 1) {
      if($obj[i].getAttribute('data-value').localeCompare(id) == 0) {
        $obj[i].remove();
        $('#Tenant').dropdown('clear');
        break;
      }
    }
  }

//Function for Tenant, change tenancy, using ajax call.
function changeTenancy() {
	//Verifying 
	if($("#oldEir").val() === "") {
    var formData = $('.ui.form.segment.tenantChange input').serialize();
    let $eirNew = $('#newEir').dropdown('get text');
    let $idNew  = $('#newEir').dropdown('get value');
    
      $("#oldEir").val($eirNew);
      $('#oldEir').text($eirNew);
      $.ajax({
        type : 'POST',
        url : '/Tenants/createTenancy',
        data : formData,
        success: 
    	  function(response){
        	document.getElementById("oldResidence").setAttribute("value", $idNew);
            reportmap.updateMarkers(response);
        	removeResidence($idNew);
        }
    });
    }
  }

//Function for tenant, removing new tenantResidence from vacantResidence dropdown.
function removeResidence(id) {
    let $obj = $('.item.residence');
    for (let i = 0; i < $obj.length; i += 1) {
      if($obj[i].getAttribute('data-value').localeCompare(id) == 0) {
        $obj[i].remove();
        $('#newEir').dropdown('clear');
        break;
      }
    }
  }

//Function for tenant, terminate tenancy, using ajax call.
function deleteTenancy() {
	  if($("#oldEir").val() !== ""){
      var formData = $('.ui.form.segment.terminate input').serialize();
      let resId = document.getElementById('oldResidence').value;
      $.ajax({
        type : 'POST',
        url : '/Tenants/terminateTenancy',
        data : formData,
        success: 
    	  function(response){
        	let newMenuItem = dropdownDiv($("#oldEir").val(), resId);
            $('.menu.vacant').append(newMenuItem);
            $('#newEir').dropdown('clear');
            $("#oldEir").val("");
            $("#oldEir").text("");
            reportmap.updateMarkers(response);
        }
       }); 
	  }
  }

//Function to create div item with eircode and resId addition to dropdown list.
function dropdownDiv(eircode, resId) {
    return '<div class="item residence"' + ' ' + 'data-value="' + resId + '">' + eircode + '</div>';
  }
   
 