package models;

import java.util.Date;
import javax.persistence.Entity;
import play.db.jpa.Model;


@Entity
public class Feedback extends Model{
    //fields that make feedback class
	public String firstName;
	public String lastName;
	public String email;
	public String message;
	public Date   sentAt;
	
	/**
	 * Constructor for objects of feedback class.
	 * 
	 * @param firstName
	 * @param lastName
	 * @param email
	 * @param message
	 */
	public Feedback(String firstName, String lastName, String email, String message ){
		this.firstName   = firstName;
		this.lastName    = lastName;
		this.email       = email;
		this.message     = message;
		sentAt           = new Date();
	}
}