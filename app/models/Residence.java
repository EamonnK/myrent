package models;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;

import play.db.jpa.Blob;
import play.db.jpa.Model;
import utils.LatLng;
import java.util.List;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;




@Entity
public class Residence extends Model{
	//fields that make residence class.
	public String geolocation;
	public String residenceType;
	public String eircode;
	public int rent;
	public int numberBedrooms;
	public int numberBathrooms;
	public int area;
	public Date date;

	//foreign key relating landlord to residence
	@ManyToOne
	public Landlord from;
	
	//foreign key relating tenant to residence.
	@OneToOne
	public Tenant occupant;
	
	/**
	 * Constructor for object of class Residence.
	 * 
	 * @param from
	 * @param geolocation
	 * @param eircode
	 * @param residenceType
	 * @param rent
	 * @param numberBedrooms
	 * @param numberBathrooms
	 * @param area
	 */
	public Residence(Landlord from, String geolocation, String eircode, String residenceType, int rent, int numberBedrooms, int numberBathrooms, int area){
		this.from            = from;
		this.geolocation     = geolocation;
		this.eircode         = eircode;
		this.residenceType   = residenceType;
		this.rent            = rent;
		this.numberBedrooms  = numberBedrooms;
		this.numberBathrooms = numberBathrooms;
		this.area            = area;
		date                 = new Date();
	}
	
	/**
	 * Method to return LatLng value of geolocation String.
	 * 
	 * @return LatLng
	 */
	public LatLng getGeolocation(){
		return LatLng.toLatLng(geolocation);
	}
	
	
}
