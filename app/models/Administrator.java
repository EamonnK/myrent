package models;

import javax.persistence.Entity;
import play.db.jpa.Model;


@Entity
public class Administrator extends Model{
	//fields that make administrator class
	String email;
	String password;
	
	/**
	 * Constructor for objects of administrator class.
	 */
	public Administrator(){
		this.email = "admin@witpress.ie";
		this.password = "secret";
	}
	
	/**
	 * Method to find administrator by email and return Administrator.
	 * 
	 * @param email
	 * @return Administrator
	 */
	public static Administrator findByEmail(String email){
	    return find("email", email).first();
	  }

	/**
	 * Method to check input password matches stored password.
	 * 
	 * @param String password
	 * @return boolean
	 */
	  public boolean checkPassword(String password){
	    return this.password.equals(password);
	  }
}