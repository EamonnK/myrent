package models;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import play.db.jpa.Blob;
import java.util.List;
import java.util.ArrayList;
import java.util.Collections;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

import controllers.Landlords;
import play.db.jpa.Model;


@Entity
public class Landlord extends Model{
  //fields that make landlord class.
  public String firstName;
  public String lastName;
  public String email;
  public String address;
  public String password;
  

  /**
   * Constructor for object of class Landlord.
   * 
   * @param firstName
   * @param lastName
   * @param email
   * @param address
   * @param password
   */
  public Landlord(String firstName, String lastName, String email, String address, String password){
    this.firstName   = firstName;
    this.lastName    = lastName;
    this.email       = email;
    this.address     = address;
    this.password    = password;
  }
  
  /**
   * Method to find landlord by email and return Landlord.
   * 
   * @param email
   * @return Landlord
   */
  public static Landlord findByEmail(String email){
    return find("email", email).first();
  }

  /**
   * Method to check that input password matches stored password.
   * 
   * @param String password
   * @return boolean
   */
  public boolean checkPassword(String password){
    return this.password.equals(password);
  }
  
}