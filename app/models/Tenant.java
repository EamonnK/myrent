package models;

import javax.persistence.Entity;
import javax.persistence.OneToOne;
import play.db.jpa.Model;

@Entity
public class Tenant extends Model{
	//fields that make tenant class
	public String firstName;
	public String lastName;
	public String email;
	public String password;
	
	//foreign key reference to residence tenant resides in
	@OneToOne(mappedBy = "occupant")
	public Residence rents;
	
	/**
	 * Constructor for object of tenant class.
	 * 
	 * @param firstName
	 * @param lastName
	 * @param email
	 * @param password
	 */
	public Tenant(String firstName, String lastName, String email, String password){
		this.firstName = firstName;
		this.lastName  = lastName;
		this.email     = email;
		this.password  = password;
	}
	
	/**
	 * Method to find tenant by email, returning matching tenant.
	 * 
	 * @param email
	 * @return Tenant
	 */
	public static Tenant findByEmail(String email){
		return find("email", email).first();
	}
	
	/**
	 * Method to check input password matches stored password.
	 * 
	 * @param String password
	 * @return boolean
	 */
	public boolean checkPassword(String password){
		return this.password.equals(password);
	}
}