package controllers;

import play.*;
import play.mvc.*;
import utils.Ascend;
import utils.Descend;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import models.*;

public class Administrators extends Controller{
	
	/**
	 * Method to render administrator login page.
	 */
	public static void login(){
		render();
	}
	
	/**
	 * Method to facilitate administrator logout, removing id from session map.
	 */
	public static void logout(){
		session.remove("logged_in_adminid");
		Welcome.index();
	}
	
	/**
	 * Method to render Administrator configuration page,
	 *  rendering all tenants and landlords to page.
	 */
	public static void configuration(){
		if(session.get("logged_in_adminid") != null){
		  List<Tenant> allTenants    = Tenant.findAll();
		  List<Landlord> allLandlords = Landlord.findAll();
		  render( allTenants, allLandlords);
		}else{
			login();
		}
	}
	
	/**
	 * Method to render register landlord page.
	 */
	public static void registerLand(){
		if(session.get("logged_in_adminid") != null){
		  render();
		}else{
			login();
		}
	}
	
	/**
	 * Method to render register tenant page.
	 */
	public static void registerTen(){
		if(session.get("logged_in_adminid") != null){
		  render();
		}else{
			login();
		}
	}
	
	/**
	 * Method to delete tenant from database, taking id from html.
	 * Updating map sending updateGeo() return as response to ajax call.
	 * 
	 * @param Long tenId
	 */
	public static void deleteTenant(Long tenId){
		Tenant ten = Tenant.findById(tenId);
		if(ten.rents != null){
		  Residence res = ten.rents;
		  res.occupant = null;
		  res.save();
		}
		ten.delete();
		renderJSON(geolocations());
	}
	
	
	/**
	 * Method to delete landlord from database, taking id from html.
	 * Updating map sending updateGeo() return as response to ajax call.
	 * 
	 * @param Long landId
	 */
	public static void deleteLandlord(Long landId){
		List<Residence> res = Residence.findAll();
		Landlord land = Landlord.findById(landId);
		for(Residence r : res){
			if(r.from == land){
				r.delete();
			}
		}
		land.delete();
		renderJSON(geolocations());
	}
	
	/**
	 * Method to render Report page rendering all residences.
	 */
	public static void report(){
		if(session.get("logged_in_adminid") != null){
		  List<Residence> residences = Residence.findAll();
		  render(residences);
		}else{
			login();
		}
	}
	
	/**
	 * Method to sort Report page table by rented or vacant status
	 * taking rentStatus string from html.
	 * 
	 * @param String rentStatus
	 */
	public static void sortByRented(String rentStatus){
		List<Residence>  unfiltered = Residence.findAll();
		List<Residence> residences   = new ArrayList();
		for(Residence r : unfiltered){
			if(rentStatus.equals("rented") && r.occupant != null){
				residences.add(r);
			}else if(rentStatus.equals("vacant") && r.occupant == null){
				residences.add(r);
			}
		}
		render("Administrators/report.html",residences);
	}
	
	/**
	 * Method to sort Report page table by residence type
	 * taking resType string from html.
	 * 
	 * @param resType
	 */
	public static void sortByType(String resType){
		List<Residence> unfiltered = Residence.findAll();
		List<Residence> residences   = new ArrayList();
		for(Residence r : unfiltered){
			if(resType.equals("house") && r.residenceType.equals("house") ){
				residences.add(r);
			}else if(resType.equals("flat") && r.residenceType.equals("flat")){
				residences.add(r);
			}else if (resType.equals("studio") && r.residenceType.equals("studio")){
				residences.add(r);
			}
		}
		render("Administrators/report.html",residences);
	}
	
	/**
	 * Method to sort Report page table by rent value, ascending and descending.
	 * 
	 * @param rentSort
	 */
	public static void sortByRent(String rentSort){
		List<Residence> residences = Residence.findAll();
		if(rentSort.equals("ascend")){
		  Collections.sort(residences, new Ascend());
		}else if(rentSort.equals("descend")){
			  Collections.sort(residences, new Descend());
			}
		render("Administrators/report.html",residences);
	}
	
	/**
	 * Method to render charts html.
	 * rendering hashmap data and list residences to page.
	 */
	public static void charts(){
		if(session.get("logged_in_adminid") != null){
		  List<Residence> residences = Residence.findAll();
		  List<Landlord>  landLords  = Landlord.findAll();
		  HashMap<String, String> data = new HashMap();
		  for(Landlord l : landLords){
		    double landRent = 0;
		    for(Residence r : residences){
			  if(l == r.from){
				  landRent += (double)r.rent;
			  } 
		    }
		  data.put(l.firstName+" "+ l.lastName, String.valueOf(landRent * 100/totalRent()));
		  }
		  render(residences, data);
		}else{
			login();
		}
	}
	
	/**
	 * Method to return total rent of all properties.
	 * 
	 * @return double totalRent
	 */
	public static double totalRent(){
		List<Residence> residences = Residence.findAll();
		double totalRent = 0;
		for(Residence r : residences){
				  totalRent += (double)r.rent;
		}
		return totalRent;
	}
	
	/**
	 * Method to authenticate administrator on login,
	 * finding administrator by email, checking if administrator exists. 
	 * If yes, does the password match and if so adding them to session map.
	 * 
	 * @param String email
	 * @param String password
	 */
	public static void authenticate(String email, String password){
		Logger.info("Attempting to authenticate with " + email + ":" +  password);
		Administrator admin = new Administrator();
		if(email.equals("admin@witpress.ie") && password.equals("secret")){
			Logger.info("Authentication successful");
		    session.put("logged_in_adminid", admin.id);
		    configuration();
		}else{
			login();
		}
	}	
	
	/**
	 * Method populating arraylist with google map marker data.
	 * Adding each resulting arrayList to JSONArray and returning this array.
	 *  
	 * @return JSONArray data
	 */
	public static JSONArray geolocations(){
		JSONArray data = new JSONArray();
		List<Residence> residences = Residence.findAll();
		for(Residence r : residences){
			ArrayList<String> geoObj = new ArrayList();
			geoObj.add( r.eircode);
			geoObj.add(String.valueOf(r.getGeolocation().getLatitude()));
			geoObj.add(String.valueOf(r.getGeolocation().getLongitude()));
			if(r.occupant == null){
				geoObj.add("No Tenant");
			}else{
			geoObj.add("Tenant is "+ r.occupant.firstName+" "+ r.occupant.lastName);
			}
			data.add(geoObj);
		}
		renderJSON(data);
		return(data);
	}
}