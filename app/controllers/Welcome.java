package controllers;

import play.*;
import play.mvc.*;

import java.util.*;

import models.*;

public class Welcome extends Controller{
	/**
	 * Method to render Welcome/home page html.
	 */
	public static void index(){
		render();
	}
}