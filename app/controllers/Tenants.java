package controllers;

import play.*;
import play.mvc.*;
import java.util.*;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import models.*;

public class Tenants extends Controller{
	/**
	 * Method to render tenant login page.
	 */
	public static void login(){
		render();
	}
	
	/**
	 * Method to facilitate tenant logout, removing id from session map.
	 */
	public static void logout(){
		session.remove("logged_in_tenantid");
		Welcome.index();
	}
	
	/**
	 * Method to render Tenant configuration page,
	 *  rendering current tenant,tenants residence and vacant Residences.
	 */
	public static void configuration(){
		Tenant tenant = getCurrentTenant();
		if(tenant != null){
		  Residence tenantRes = tenant.rents;
		  List<Residence> vacant = new ArrayList<Residence>();
		  List<Residence> allResidences = Residence.findAll();
		  for(Residence res : allResidences){
			if(res.occupant == null){
				vacant.add(res);
			}
		  }
		  render(tenant, tenantRes, vacant);
		}else{
			login();
		}
	}
	
	/**
	 * Method to terminate current Tenancy.
	 */
	public static void terminateTenancy(){
		Tenant tenant = getCurrentTenant();
		tenant.rents.occupant = null;
		tenant.rents.save();
		tenant.rents = null;
		tenant.save();
		renderJSON(geolocations());
	}
	
	/**
	 * Method to create new Tenancy, using resId value from html.
	 * 
	 * @param Long resId
	 */
	public static void createTenancy(Long resId){
		Tenant tenant = getCurrentTenant();
		tenant.rents = Residence.findById(resId);
		tenant.save();
		tenant.rents.occupant= tenant;
		tenant.rents.save();
		renderJSON(geolocations());
	}
	
	/**
	 * Method to register a new tenant, creating new tenant object with fields
	 * corresponding to tenant class.
	 * 
	 * @param String firstName
	 * @param String lastName
	 * @param String email
	 * @param String password
	 */
	public static void register(String firstName, String lastName, String email, String password) {
	    Logger.info(firstName + " " + lastName + " " + email + " " + password );
	    Tenant tenant = new Tenant(firstName, lastName, email, password);
	    tenant.save();
	    Administrators.configuration();  
	}
	
	/**
	 * Method to authenticate tenant on login,
	 * finding tenant by email, checking if tenant exists. 
	 * If yes, does the password match and if so adding them to session map.
	 * 
	 * @param String email
	 * @param String password
	 */
	public static void authenticate(String email, String password) {
	    Logger.info("Attempting to authenticate with " + email + ":" +  password);
	    Tenant tenant = Tenant.findByEmail(email);
	    
	    if ((tenant != null) && (tenant.checkPassword(password) == true)){
	      Logger.info("Authentication successful");
	      session.put("logged_in_tenantid", tenant.id);
	      Tenants.configuration();
	    }
	    else{
	      Logger.info("Authentication failed");
	      Tenants.login();  
	    }
	 }
	
	/**
	 * Method to find current tenant by logged_in_tenantid.
	 * returning Tenant logged_in_tenantid.
	 * 
	 * @return Tenant logged_in_tenant
	 */
	public static Tenant getCurrentTenant(){
		 String tenantId = session.get("logged_in_tenantid");
		 if(tenantId == null){
			 return null;
		 }
		 Tenant logged_in_tenant = Tenant.findById(Long.parseLong(tenantId));
		 Logger.info("Logged in user is "+ logged_in_tenant.firstName);
		 return logged_in_tenant;	 
	 }	
	
	/**
	 * Method to add ArrayLists of residence details to JSONArray.
	 * 
	 * @return JSONArray data
	 */
	public static JSONArray geolocations(){
		JSONArray data = new JSONArray();
		List<Residence> residences = Residence.findAll();
		for(Residence r : residences){
			if(r.occupant == null){ 
			  ArrayList<String> geoObj = new ArrayList();			
			  geoObj.add( r.eircode);
			  geoObj.add(String.valueOf(r.getGeolocation().getLatitude()));
			  geoObj.add(String.valueOf(r.getGeolocation().getLongitude()));
			  geoObj.add("No Tenant");
			  geoObj.add(String.valueOf(r.date));
			  geoObj.add(r.residenceType);
			  geoObj.add(String.valueOf(r.numberBedrooms));
			  geoObj.add(String.valueOf(r.numberBathrooms));
			  geoObj.add(String.valueOf(r.rent));
			  geoObj.add(String.valueOf(r.area));
			  geoObj.add(r.from.firstName + " " + r.from.lastName);
			  	
			  data.add(geoObj);
			}
		}
		renderJSON(data);
		return(data);
	}
}