package controllers;

import play.*;
import play.mvc.*;
import java.util.*;
import models.*;

public class Contact extends Controller{
	/**
	 * Method to render contact html.
	 */
	public static void contact(){
		render();
	}
	
	/**
	 * Method to render acknowledgement page on sending of message.
	 */
	public static void acknowledgement(){
		render();
	}
	
	/**
	 * Method to allow users to contact website by creating an object of class Feedback ,
	 *  only allowing message if all fields are filled.
	 * 
	 * @param firstName
	 * @param lastName
	 * @param email
	 * @param message
	 */
	public static void sendFeedback(String firstName, String lastName, String email, String message ){
	    Feedback feed = new Feedback(firstName, lastName, email, message);
	    feed.save();
	    acknowledgement();
	}
}