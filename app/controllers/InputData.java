package controllers;

import play.*;
import play.mvc.*;
import utils.LatLng;

import java.util.*;
import models.*;

public class InputData extends Controller{
	
	/**
	 * Method to render input data index html,
	 * checking to verify landlord is logged in.
	 */
	public static void index(){
		Landlord landlord = Landlords.getCurrentLandLord();
		if(landlord == null){
		    Logger.info("Unable to getCurrentUser");
		    Landlords.login();
		}
		else{
			render();
		}
	}
	
	/**
	 * Method to capture data relating to a new property, creating a residence object with input fields
	 * and saving to database.
	 * 
	 * @param geolocation
	 * @param eircode
	 * @param residenceType
	 * @param rent
	 * @param numberBedrooms
	 * @param numberBathrooms
	 * @param area
	 */
	public  static void dataCapture(String geolocation,String eircode, String residenceType, int rent, int numberBedrooms, int numberBathrooms, int area){
		
		Logger.info("Location :" + geolocation + " Type:" + residenceType + " Rent:" + rent + " Bedrooms:" + numberBedrooms + " Bathrooms:" + numberBathrooms + "Area(m):" + area);
		Landlord landlord = Landlords.getCurrentLandLord();
		
		if(landlord == null){
			Logger.info("Unable to getCurrentUser");
		    Landlords.login();
		}
		else{
			addResidence(landlord, geolocation, eircode, residenceType, rent, numberBedrooms, numberBathrooms, area);
			index();
		}
	}
		
	/**
	 * Method to create and save a new residence, called in the method above.
	 * 
	 * @param from
	 * @param geolocation
	 * @param eircode
	 * @param residenceType
	 * @param rent
	 * @param numberBedrooms
	 * @param numberBathrooms
	 * @param area
	 */
	private static void addResidence(Landlord from, String geolocation, String eircode, String residenceType, int rent, int numberBedrooms, int numberBathrooms, int area){
		
		Residence res = new Residence(from, geolocation, eircode, residenceType, rent, numberBedrooms, numberBathrooms, area);
		res.save();
	}
		
}