package controllers;

import play.*;
import play.mvc.*;
import java.util.*;
import models.*;

public class Landlords extends Controller {
	
	/**
	 * Method to render landlord login page.
	 */
    public static void login(){
    	render();
    }

    /**
	 * Method to facilitate landlord logout, removing id from session map.
	 */
	public static void logout(){
	    session.remove("logged_in_landlordid");
	    Welcome.index();
	}
	
	/**
	 * Method to render Landlord configuration page,
	 *  rendering current landlord and their residences.
	 */
	public static void configuration(){
		Landlord landLord           = getCurrentLandLord();
		if(landLord != null){
		  List<Residence> allRes      = Residence.findAll();
		  List<Residence> landLordRes = new ArrayList<Residence>(); 
		  for(Residence res : allRes){
			if(res.from == landLord){
				landLordRes.add(res);
			}
		  }
		  render(landLord,landLordRes);
		}else{
			login();
		}
		
	}
	
	/**
	 * Method to delete a residence from database using resId string input from html.
	 * 
	 * @param resId
	 */
	public static void deleteResidence(Long resId){
		Residence res = Residence.findById(resId);
		res.delete();
		configuration();
	}
	
	/**
	 * Method to render editResidence html, using input resId from html to render 
	 * appropriate residence.
	 * 
	 * @param resId
	 */
	public static void editResidence(Long resId){
		Landlord landLord           = getCurrentLandLord();
		if(landLord != null){
		  Residence res = Residence.findById(resId);
		  render(res);
		}else{
			login();
		}
	}
	
	/**
	 * Method to edit rent field relating to selected residence
	 * leaving field unchanged if there are no alterations.
	 * 
	 * @param rent
	 * @param eircode
	 * @param resId
	 */
	public static void editRes(int rent, String eircode, Long resId){
		Residence res = Residence.findById(resId);
		res.rent = (rent == 0)? res.rent : rent; 
	    res.save();
	    configuration();
	}
	
	/**
	 * Method to render landlord edit profile page,
	 * rendering landlord and their address to page.
	 */
	public static void edit(){
		Landlord landLord           = getCurrentLandLord();
		if(landLord != null){
		  String[] landAdd  = landLord.address.split(",");
		  render(landLord, landAdd);
		}else{
			login();
		}
	}
	
	/**
	 * Method to register a landlord accessed only by administrator, 
	 * creating a landlord object and saving to database.
	 * 
	 * @param firstName
	 * @param lastName
	 * @param email
	 * @param address
	 * @param password
	 */
	public static void register(String firstName, String lastName, String email, String address, String password) {
		
	    Logger.info(firstName + " " + lastName + " " + email + " " +" " + password);
	    Landlord landlord = new Landlord(firstName, lastName, email, address, password);
	    landlord.save();
	    Administrators.configuration();   
	 }
	
	/**
	 * Method to authenticate landlord on login,
	 * finding landlord by email, checking if landlord exists. 
	 * If yes, does the password match and if so adding them to session map.
	 * 
	 * @param String email
	 * @param String password
	 */
	 public static void authenticate(String email, String password) {
		 
	    Logger.info("Attempting to authenticate with " + email + ":" +  password);
	    Landlord landlord = Landlord.findByEmail(email);
	    if ((landlord != null) && (landlord.checkPassword(password) == true)){
	      Logger.info("Authentication successful");
	      session.put("logged_in_landlordid", landlord.id);
	      configuration();
	    }
	    else{
	      Logger.info("Authentication failed");
	      login();  
	    }
	 }
	 
	 /**
	  * Method to find current landlord by logged_in_landlord
	  * 
	  * @return Landlord logged_in_landlord
	  */
	 public static Landlord getCurrentLandLord(){
		 
		 String landLordId = session.get("logged_in_landlordid");
		 if(landLordId == null){
			 return null;
		 }
		 Landlord logged_in_landlord = Landlord.findById(Long.parseLong(landLordId));
		 Logger.info("Logged in landlord is "+ logged_in_landlord.firstName);
		 return logged_in_landlord;
		 
	 }
	 
	 /**
	  * Method to edit landlord profile details, allowing for fields left empty to remain the same.
	  * only implementing changes if entered password matches landlord password.
	  * 
	  * @param String firstName
	  * @param String lastName
	  * @param String email
	  * @param String[] address
	  * @param String password
	  */
	 public static void editDetails(String firstName, String lastName, String email, String[] address, String password){
		 
		  Landlord landLord = getCurrentLandLord();
		  String[] oldAdd   = landLord.address.split(",");
		  
		  landLord.firstName     = (firstName.equals(""))    ? landLord.firstName   : firstName;
		  landLord.lastName      = (lastName.equals(""))     ? landLord.lastName    : lastName;
		  landLord.email         = (email.equals(""))        ? landLord.email       : email;
		  String s1              = (address[0].equals(""))   ? oldAdd[0]            : address[0];
		  String s2              = (address[1].equals(""))   ? oldAdd[1]            : address[1];
		  String s3              = (address[2].equals(""))   ? oldAdd[2]            : address[2];
		  String s4              = (address[3].equals(""))   ? oldAdd[3]            : address[3];
		  String s5              = (address[4].equals(""))   ? oldAdd[4]            : address[4];
		  landLord.address       = (s1 + "," + s2 + "," + s3 + "," + s4 + "," + s5);
		
	      if(landLord.password.equals(password)){
	    	  landLord.save();
	    	  configuration();
	      }
	  }
	 
}