package utils;
import java.util.Comparator;
import models.*;

public class Ascend implements Comparator<Residence>{

	/**
	 * Method to sort residences by rent field in ascending order.
	 */
	@Override
	public int compare(Residence a, Residence b) {
		return  Integer.compare(a.rent, b.rent);
	}
	
}