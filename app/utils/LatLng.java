package utils;



public class LatLng
{

  private double lat;
  private double lng;

  /**
   * Constructor for object LatLng
   * 
   * @param lat
   * @param lng
   */
  public LatLng(double lat, double lng)
  {
    this.lat = lat;
    this.lng = lng;
  }

  /**
   * Method to return string value of lat and lng.
   * 
   * @return String
   */
  public String toString()
  {
    return lat + "," + lng;
  }

  /**
   * @param latlng
   *          : a string comprising a lat,lng : eg 53.444,-5.455
   * @return a LatLng object whose fields obtained by parsing argument
   */
  public static LatLng toLatLng(String latlng)
  {
    String[] latLng = latlng.split(",");
    return new LatLng(Double.parseDouble(latLng[0]), Double.parseDouble(latLng[1]));
  }

  /**
   * Method to return latitude
   * 
   * @return double 
   */
  public double getLatitude()
  {
    return lat;
  }

  /**
   * Method to return longitude
   * 
   * @return double 
   */
  public double getLongitude()
  {
    return lng;
  }

}