package utils;
import java.util.Comparator;
import models.*;

public class Descend implements Comparator<Residence>{

	/**
	 * Method to sort residences by rent field in descending order.
	 */
	@Override
	public int compare(Residence a, Residence b) {
		return  Integer.compare(b.rent, a.rent);
	}
	
}